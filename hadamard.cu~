#include <iostream>
#include <cstdlib>
#include <cmath>
#include <chrono>
#include <algorithm>
#include <geiger/geiger.h>
#include <cuda_occupancy.h>

__global__ void hadamard(size_t n, float* a, float* b, float* c)
{
    int const id = blockIdx.x * blockDim.x + threadIdx.x;
    if(id < n)
	c[id] = a[id] * b[id];
}

__global__ void hadamard_fast(size_t n, float* a, float* b, float* c)
{
    int const id = blockIdx.x * blockDim.x + threadIdx.x;
    if(id < n)
	c[id] = __fmaf_rn(a[id], b[id], 0);
}

__global__ void hadamardnadd_fast(size_t n, float* a, float* b, float* c)
{
    int const id = blockIdx.x * blockDim.x + threadIdx.x;
    if(id < n)
	c[id] = __fmul_rn(a[id], b[id]);
}

__global__ void hadamard_coal(size_t n, float* a, float* b, float* c)
{
    int const id = blockIdx.x * blockDim.x + threadIdx.x;
    __shared__ float shared_a[64];
    __shared__ float shared_b[64];
    __shared__ float shared_c[64];

    shared_a[threadIdx.x] = a[id];
    shared_b[threadIdx.x] = b[id];
    shared_c[threadIdx.x] = c[id];
    __syncthreads();

    if(id < n)
	shared_c[threadIdx.x] = __fmul_rn(shared_a[threadIdx.x], shared_b[threadIdx.x]);
    __syncthreads();

    c[id] = shared_c[threadIdx.x];
}

__global__ void hadamard_dyn_coal(size_t n, float* a, float* b, float* c)
{
    int const id = blockIdx.x * blockDim.x + threadIdx.x;
    extern __shared__ float shared[];

    if(id < n)
    {
	shared[id] = a[id];
	shared[blockDim.x + id] = b[id];
	shared[blockDim.x * 2 + id] = c[id];
    }
    __syncthreads();

    if(id < n)
	shared[blockDim.x  * 2 + id] = __fmul_rn(shared[id], shared[blockDim.x + id]);
    __syncthreads();

    c[id] = shared[blockDim.x * 2 + id];
}

int main()
{
    geiger::init();
    geiger::suite<> suite;

    size_t size = 1000;

    float* a = (float*)malloc(sizeof(float)*size);
    float* b = (float*)malloc(sizeof(float)*size);
    float* c = (float*)malloc(sizeof(float)*size);

    std::fill(a, a + size, 5);
    std::fill(b, b + size, 3);

    float* d_a;
    float* d_b;
    float* d_c;

    cudaMalloc(&d_a, sizeof(float)*size);
    cudaMalloc(&d_b, sizeof(float)*size);
    cudaMalloc(&d_c, sizeof(float)*size);

    cudaMemcpy(d_a, a, sizeof(float) * size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(float) * size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_c, c, sizeof(float) * size, cudaMemcpyHostToDevice);

    int block_size = 64;
    int grid_size = (int)ceil((float)size/block_size);
    suite.add("hadamard standard",
	      [d_a, d_b, d_c, size]()
	      {
		  int grid;
		  int block;
		  cudaOccupancyMaxPotentialBlockSize(&grid, &block, hadamard);
		  grid= (int)ceil((float)size/block);
		  hadamard<<<grid, block>>>(size, d_a, d_b, d_c);
	      })
	.add("hadamard fast",
	     [d_a, d_b, d_c, size]()
	     {
		 int grid;
		 int block;
		 cudaOccupancyMaxPotentialBlockSize(&grid, &block, hadamard_fast);
		 grid= (int)ceil((float)size/block);
		 hadamard_fast<<<grid, block>>>(size, d_a, d_b, d_c);
	     })
	.add("hadamard and add",
	     [d_a, d_b, d_c, size]()
	     {
		 int grid;
		 int block;
		 cudaOccupancyMaxPotentialBlockSize(&grid, &block, hadamardnadd_fast);
		 grid= (int)ceil((float)size/block);
		 hadamardnadd_fast<<<grid, block>>>(size, d_a, d_b, d_c);
	     })
	.add("hadamard with coalescing",
	     [d_a, d_b, d_c, size, block_size, grid_size]()
	     {
		 hadamard_coal<<<grid_size, block_size>>>(size, d_a, d_b, d_c);
	     })
	.add("hadamard with coalescing dynamic shared memory",
	     [d_a, d_b, d_c, size, block_size, grid_size]()
	     {
		 int grid;
		 int block;
		 cudaOccupancyMaxPotentialBlockSize(&grid, &block, hadamard_dyn_coal);
		 grid = (int)ceil((float)size/block);
		 hadamard_coal<<<grid, block, block * 3>>>(size, d_a, d_b, d_c);
	     })
	.set_printer<geiger::printer::console<>>()
	.run(std::chrono::milliseconds(1));

    cudaMemcpy(c, d_c, sizeof(float) * size, cudaMemcpyDeviceToHost);
    
    std::cout << "result: " << c[0] << std::endl;

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
    free(a);
    free(b);
    free(c);

    return 0;
}