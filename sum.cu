#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <stdint.h>
#include <cmath>
#include <chrono>
#include <algorithm>
#include <numeric>
#include <geiger/geiger.h>
#include <cuda_occupancy.h>

#define BLK 64
#define WARP_SIZE 32

__global__ void reduce_stupid(float* a, float* result, size_t n)
{
    int const id = blockIdx.x * blockDim.x + threadIdx.x;

    if(id >= n)
	return;

    atomicAdd(result, a[id]);
}
__global__ void reduce_first(float* a, float* result, size_t n)
{
    __shared__ int shared[BLK];

    uint32_t const tid = threadIdx.x;
    int const id = blockIdx.x * blockDim.x + threadIdx.x;

    if(id >= n)
	return;

    shared[tid] = a[id];
    __syncthreads();

    if(blockIdx.x == gridDim.x - 1)
    {
	atomicAdd(result, shared[tid]);
	return;
    }
    __syncthreads();

    for(uint32_t stride = blockDim.x / 2; stride > 0u; stride >>= 1)
    {
	if(tid < stride)
	{
	    shared[tid] += shared[tid + stride];
	}
	__syncthreads();
    }
			  
    if(tid == 0u)
    {
	atomicAdd(result, shared[0]); 
    }
}

__global__ void reduce_second(float* a, float* result, size_t n)
{
    size_t const tid = threadIdx.x;
    size_t const id = blockIdx.x * blockDim.x + threadIdx.x;

    __shared__ float shared_sum;
    shared_sum = 0;
    int sum = id < n ? a[id] : 0;
    __syncthreads();

    for(size_t i = WARP_SIZE / 2; i >= 1u; i >>= 1)
    {
	sum += __shfl_down(sum, i);
    }

    if(threadIdx.x % WARP_SIZE == 0)
	atomicAdd(&shared_sum, sum);
    __syncthreads();

    if(threadIdx.x == 0)
	atomicAdd(result, shared_sum);
}

int main()
{
    geiger::init();
    geiger::suite<> suite;

    size_t size = 1000;

    float result = 0;

    float* a = (float*)malloc(sizeof(float)*size);
    std::fill(a, a + size, 5);

    float* d_a;
    cudaMalloc(&d_a, sizeof(float) * size);
    cudaMemcpy(d_a, a, sizeof(float) * size, cudaMemcpyHostToDevice);

    float* d_result_stupid;
    cudaMalloc(&d_result_stupid, sizeof(float));
    cudaMemset(d_result_stupid, (float)0, sizeof(float));

    float* d_result_first;
    cudaMalloc(&d_result_first, sizeof(float));
    cudaMemset(d_result_first, (float)0, sizeof(float));

    float* d_result_second;
    cudaMalloc(&d_result_second, sizeof(float));
    cudaMemset(d_result_second, (float)0, sizeof(float));

    suite.add("reduce cpu", [a, &result, size]()
	      {
		  result = std::accumulate(a, a + size, 0.0);
	      })
	.add("reduce stupid",
	     [d_a, &d_result_stupid, size]()
	     {
		 int grid;
		 int block;
		 cudaOccupancyMaxPotentialBlockSize(&grid, &block, reduce_stupid);
		 grid = (int)ceil((float)size/block);
		 reduce_stupid<<<grid, block>>>(d_a, d_result_stupid, size);
	     })
	.add("reduce first",
	     [d_a, &d_result_first, size]()
	     {
		 int grid_size = (int)ceil((float)size/BLK);
		 reduce_first<<<grid_size, BLK>>>(d_a, d_result_first, size);
	     })
	.add("reduce first",
	     [d_a, &d_result_second, size]()
	     {
		 int grid_size = (int)ceil((float)size/BLK);
		 reduce_second<<<grid_size, BLK>>>(d_a, d_result_second, size);
	     })
	.set_printer<geiger::printer::console<>>()
	.run(1000);

    std::cout << "result: " << result << std::endl;

    cudaMemcpy(&result, d_result_stupid, sizeof(float), cudaMemcpyDeviceToHost);
    std::cout << "result stupid: " << result << std::endl;

    cudaMemcpy(&result, d_result_first, sizeof(float), cudaMemcpyDeviceToHost);
    std::cout << "result_first: " << result << std::endl;

    cudaMemcpy(&result, d_result_second, sizeof(float), cudaMemcpyDeviceToHost);
    std::cout << "result_second: " << result << std::endl;

    cudaFree(d_a);
    cudaFree(d_result_stupid);
    cudaFree(d_result_first);
    cudaFree(d_result_second);
    free(a);

    return 0;
}