#include <iostream>
#include <cstdlib>
#include <cmath>
#include <chrono>
#include <algorithm>
#include <geiger/geiger.h>
#include <cuda_occupancy.h>

__global__ void fill(float* arr, float value, size_t n)
{
    int const id = blockIdx.x * blockDim.x + threadIdx.x;
    if(id >= n)
	return;
    arr[id] = value;
}

__global__ void fill_coal(float* arr, float value, size_t n)
{
    int const id = blockIdx.x * blockDim.x + threadIdx.x;
    __shared__ float shared[64];

    if(id >= n)
	return;

    shared[threadIdx.x] = value;
    __syncthreads();

    arr[id] = shared[threadIdx.x];
}

__global__ void fill_dyn_coal(float* arr, float value, size_t n)
{
    int const id = blockIdx.x * blockDim.x + threadIdx.x;
    extern __shared__ float shared[];

    if(id >= n)
	return;

    shared[threadIdx.x] = value;
    __syncthreads();

    arr[id] = shared[threadIdx.x];
}

int main()
{
    geiger::init();
    geiger::suite<> suite;

    size_t const size = 10000000;
    float value = 7;

    float* d_a;
    float* d_b;
    float* d_c;

    cudaMalloc(&d_a, sizeof(float)*size);
    cudaMalloc(&d_b, sizeof(float)*size);
    cudaMalloc(&d_c, sizeof(float)*size);

    int block_size = 64;
    int grid_size = (int)ceil((float)size/block_size);
    suite.add("fill",
	      [d_a, value, size]()
	      {
		  int grid;
		  int block;
		  cudaOccupancyMaxPotentialBlockSize(&grid, &block, fill);
		  grid = (int)ceil((float)size/block);
		  fill<<<grid, block>>>(d_a, value, size);
	      })
	.add("fill with coalescing",
	     [d_b, value, size, block_size, grid_size]()
	     {
		 fill_coal<<<grid_size, block_size>>>(d_b, value, size);
	     })
	.add("fill with coalescing dynamic shared memory",
	     [d_c, value, size, block_size, grid_size]()
	     {
		 int grid;
		 int block;
		 cudaOccupancyMaxPotentialBlockSize(&grid, &block, fill_dyn_coal);
		 grid = (int)ceil((float)size/block);
		 fill_dyn_coal<<<grid, block, block * sizeof(float)>>>(d_c, value, size);
	     })
	.set_printer<geiger::printer::console<>>()
	.run(std::chrono::milliseconds(1));


    float* a = (float*)malloc(sizeof(float) * size);
    float* b = (float*)malloc(sizeof(float) * size);
    float* c = (float*)malloc(sizeof(float) * size);

    cudaMemcpy(a, d_a, sizeof(float) * size, cudaMemcpyDeviceToHost);
    cudaMemcpy(b, d_b, sizeof(float) * size, cudaMemcpyDeviceToHost);
    cudaMemcpy(c, d_c, sizeof(float) * size, cudaMemcpyDeviceToHost);

    for(auto i = 0u; i < size; ++i)
    {
	if(a[i] != value)
	    std::cout << "fuck!\n";
    }
    for(auto i = 0u; i < size; ++i)
    {
	if(b[i] != value)
	    std::cout << "fuck!\n";
    }
    for(auto i = 0u; i < size; ++i)
    {
	if(c[i] != value)
	    std::cout << "fuck!\n";
    }
    
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
    free(a);
    free(b);
    free(c);

    return 0;
}